package main

import "testing"

func TestFpb(t *testing.T) {
	var kue, apel int
	kue = 20
	apel = 25
	nilaiSeharusnya := 5

	resultKueApel := Fpb(kue, apel)

	if resultKueApel != nilaiSeharusnya {
		//unit test failed
		t.Fatal("Nilai seharusnya ", nilaiSeharusnya)
	}
}

func TestPerBungkus(t *testing.T) {
	var kue, apel int
	kue = 20
	apel = 25
	nilaiSeharusnyaK := 4
	nilaiSeharusnyaA := 5

	resultKueApel := Fpb(kue, apel)
	resultPerK, resultPerA := PerBungkus(kue, apel, resultKueApel)
	if resultPerK != nilaiSeharusnyaK {
		//unit test failed
		t.Fatal("Nilai seharusnya kue adalah", nilaiSeharusnyaK)
	}
	if resultPerA != nilaiSeharusnyaA {
		//unit test failed
		t.Fatal("Nilai seharusnya kue adalah", nilaiSeharusnyaA)
	}
}
