package main

import (
	"fmt"
)

func main() {
	var kue, apel int
	kue = 20
	apel = 25
	resultKueApel := Fpb(kue, apel)
	resultPerK, resultPerA := PerBungkus(kue, apel, resultKueApel)
	fmt.Println("Dengan banyaknya ", kue, "kue", " dan ", apel, " apel dapat dibungkus kotak sebanyak ", resultKueApel, "kotak")
	fmt.Println("Isi setiap kotak berisi apel + kue sebanyak", resultPerK+resultPerA)
	fmt.Println("Masing - masing kue dan apel tiap bungkus berisi ", resultPerK, " kue dan ", resultPerA, "apel")
}

func Fpb(kue int, apel int) int {

	var nKecil int
	if kue < apel {
		nKecil = kue
	} else {
		nKecil = apel
	}
	fpb := kue % apel

	fpb = nKecil
	for i := 0; i < nKecil; i++ {
		if kue%fpb != 0 || apel%fpb != 0 {
			fpb--
		}
	}
	return fpb
}

func PerBungkus(kue int, apel int, resultKueApel int) (int, int) {
	var peBungkusK, peBungkusA int
	peBungkusK = kue / resultKueApel
	peBungkusA = apel / resultKueApel

	return peBungkusK, peBungkusA
}
